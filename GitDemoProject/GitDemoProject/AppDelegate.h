//
//  AppDelegate.h
//  GitDemoProject
//
//  Created by Mayur Sardana on 10/06/15.
//  Copyright (c) 2015 Mayur Sardana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

