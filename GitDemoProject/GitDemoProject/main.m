//
//  main.m
//  GitDemoProject
//
//  Created by Mayur Sardana on 10/06/15.
//  Copyright (c) 2015 Mayur Sardana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
